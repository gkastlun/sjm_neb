import sys
from ase.parallel import world
from ase.optimize import BFGS
from ase.visualize import view
from gpaw import FermiDirac
from ase.io import write, read
from ase.units import Pascal, m, Bohr
# from ase.neb import NEB
try:
    from dynamic_NEB import NEB
except:
    sys.path.insert(1, '/gpfs/data/ap31/scripts')
    from dynamic_NEB import NEB


# Import solvation modules
from ase.data.vdw import vdw_radii
from gpaw.solvation import (LinearDielectric, GradientSurface,
                            SurfaceInteraction, EffectivePotentialCavity)

from gpaw.solvation.sjm import SJM, SJMPower12Potential

"""
Solvent parameters from JCP 141, 174108 (2014):
"""
u0 = 0.180                          # eV
epsinf = 78.36                      # dielectric constant of water at 298K
gamma = 18.4 * 1e-3 * Pascal * m    # Surface tension
T = 298.15                          # Temperature
vdw_radii = vdw_radii.copy()
atomic_radii = lambda atoms: [vdw_radii[n] for n in atoms.numbers]

# NEB parameters
nimg = 1            # Number of neb images
potential = None     # Desired potential

# Perform interpolation between IS and FS for NEB start
fresh_start = False
# Relax the initial and final state
# Options are: Bool,'IS','FS','only','FSonly','ISonly'
# Bool: Relax both initial (IS) and final (FS) states
# 'IS','FS': Relax only the given endstate
# 'only' in the name tells the script to only relax the endstates
relax_end_states = True
# Turn climbing in NEB on or off
climb = False
# Write out the electron density of the endstates
write_cube = True
# Write out the cavity,elstat. potential and bckgrnd_charge
verbose = True

# Some parameters regarding filenames and initial guess of charges
if fresh_start:
    initial_file = 'IS_start.traj'
    final_file = 'FS_start.traj'
    ne_IS = -0.4  # First guess of charge on IS
    ne_FS = -0.3  # First guess of charge on FS
    restart_file = None
else:
    restart_file = 'neb_start.traj'


# Calculator
def calculator(txtfile):
    """Obviously this calculator should be adapted to personal needs"""
    return SJM(doublelayer={'thickness': 3},
               poissonsolver={'dipolelayer': 'xy'},
               gpts=(48, 32, 144),
               kpts=(4, 6, 1),
               xc='PBE',
               txt=txtfile,
               verbose=verbose,
               spinpol=False,
               potential=potential,
               occupations=FermiDirac(0.1),
               maxiter=400,
               cavity=EffectivePotentialCavity(
                   effective_potential=SJMPower12Potential(atomic_radii,
                                                           u0,
                                                           H2O_layer=False),
                   temperature=T,
                   surface_calculator=GradientSurface()),
               dielectric=LinearDielectric(epsinf=epsinf),
               interactions=[SurfaceInteraction(surface_tension=gamma)])


# Setting up the images
if restart_file:
    index = '-%i:' % (nimg + 2)
    images = read(restart_file, index=index)
    try:
        # This needs the adaptation in ase as described in the bitbucket of SJM
        ne_img = [float(image.calc.results['ne']) for image in images]
    except:
        # Very bad initial guesses! Should be exchanged by actual values
        ne_img = list(range(nimg + 2) / 10.)

else:
    both = True
    if isinstance(relax_end_states, str):
        if relax_end_states[-4:] == 'only':
            if relax_end_states[0:2] == 'IS':
                both = False
                initial = read(initial_file)
                if initial.calc is not None:
                    if 'ne' in initial.calc.results.keys():
                        ne_IS = initial.calc.results['ne']
                images = [initial]
                ne_img = [ne_IS]
            elif relax_end_states[0:2] == 'FS':
                both = False
                final = read(final_file)
                if final.calc is not None:
                    if 'ne' in final.calc.results.keys():
                        ne_FS = final.calc.results['ne']
                images = [final]
                ne_img = [ne_FS]

    if both:
        initial = read(initial_file)
        final = read(final_file)
        if initial.calc is not None:
            if 'ne' in initial.calc.results.keys():
                ne_IS = initial.calc.results['ne']
        if final.calc is not None:
            if 'ne' in final.calc.results.keys():
                ne_FS = final.calc.results['ne']

    # Shifts atoms in z direction so the lowest layer is identical in all
    # images
        initial.translate([0, 0, -initial.positions[0][2] +
                          final.positions[0][2]])

        images = [initial]
        ne_img = [ne_IS]

        for i in range(nimg):
            images.append(initial.copy())
            ne_img.append(ne_IS + (ne_FS - ne_IS) * (i + 1) / float(nimg + 1))

        images.append(final)
        ne_img.append(ne_FS)

# If the endstates should be relaxed in the same run
if relax_end_states:
    endstates = [0, -1]
    if isinstance(relax_end_states, str):
        if relax_end_states[0:2] == 'IS':
            endstates = [0]
        elif relax_end_states[0:2] == 'FS':
            endstates = [-1]

    system = ['IS', 'FS']

    for i in endstates:
        images[i].set_calculator(calculator(system[i] + '.txt'))
        images[i].calc.ne = ne_img[i]

        qn = BFGS(images[i], logfile=system[i] + '.log',
                  trajectory=system[i] + '.traj')
        qn.run(fmax=0.03)

        write(system[i] + '_relaxed_pot_%1.2f_ne_%1.5f.traj'
              % (images[i].calc.results['electrode_potential'],
                 images[i].calc.ne), images[i], format='traj')
        if write_cube:
            rho = images[i].calc.get_all_electron_density(gridrefinement=2) * \
                Bohr ** 3
            write(system[i] + '_density.cube', images[i], data=rho)

    if isinstance(relax_end_states, str):
        if relax_end_states[-4:] == 'only':
            sys.exit()
else:
    for i in [0, -1]:
        images[i].calc.atoms = images[i]
        images[i].calc.ne = ne_img[i]

# Combine NEB images with their respective calculators
for i in range(1, nimg + 1):
        images[i].set_calculator(calculator('image_%i.txt' % (i)))
        images[i].calc.ne = ne_img[i]

# Run the NEB
neb = NEB(images, climb=climb)

if not restart_file:
    neb.interpolate()

if world.size == 1:
    view(images)
    sys.exit()
else:
    qn = BFGS(neb, logfile='neb.log', trajectory='neb.traj')
    qn.run(fmax=0.05)

write('neb_GC_final.traj', images)
